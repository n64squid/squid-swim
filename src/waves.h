#include <libdragon.h>

#define WAVE_OFFSET_X 2

typedef struct wave_segment_s {
	float scale_x;
	float scale_y;
	float pos_x;
	float pos_y;
	uint8_t type;
	struct wave_segment_s* next;
} wave_segment_t;

typedef struct {
	wave_segment_t* start;
	wave_segment_t* end;
} wave_ll_t;

typedef enum {
	WAVE_VALUE_POS_Y,
	WAVE_VALUE_THETA,
	WAVE_VALUE_TYPE,
} wave_value_t;

void waves_draw(void);
void waves_init(void);
void waves_add (wave_ll_t* w);
float waves_get_value(float x, wave_value_t type);
