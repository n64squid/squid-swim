# Set common terminal settings
set terminal pngcairo size 102,102 enhanced font 'Verdana,10'
unset key
unset xtics
unset ytics
unset border
set lmargin 0
set rmargin 0
set tmargin 0
set bmargin 0
set xrange [0:pi/2]
set yrange [-0.025:1.025]
set samples 1000
set style fill solid 0.5

# Function to plot the sine curve
plot_sine(filename) = sprintf("set output '%s'; set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb '#88abf7' behind; plot sin(x) with filledcurves x1 lc rgb '#2E2252', sin(x) with lines lw 4 lc rgb '#224952'", filename)

# Normal sine curve
eval plot_sine("ocean_0.png")

# Flip horizontally (reverse x-axis)
set xrange [pi/2:0]
eval plot_sine("ocean_1.png")

# Flip vertically (reverse y-axis)
set xrange [0:pi/2]
set yrange [1.025:-0.025]
eval plot_sine("ocean_2.png")

# Flip both horizontally and vertically
set xrange [pi/2:0]
set yrange [1.025:-0.025]
eval plot_sine("ocean_3.png")

unset output
