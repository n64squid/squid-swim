typedef struct squid_s {
	float x;
	float y;
	float momentum;
	float theta;
	uint32_t time_in_air;
	sprite_t* sprite;
	uint8_t slice;
	rdpq_blitparms_t blitparms;
} squid_t;

extern squid_t* squid;

squid_t* squid_init(void);
void squid_handle_input(void);
void squid_move(squid_t* s);
void squid_draw(squid_t* s);
void squid_delete(squid_t** s);
