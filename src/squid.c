#include <libdragon.h>
#include "squid.h"
#include "waves.h"

#define SQUID_SLICE_X 32
#define SQUID_SLICE_Y 16
#define SQUID_SLICE_MAX 3

#define SQUID_DECAY_MOMENTUM_UPHILL (1.0/64)
#define SQUID_MIN_MOMENTUM 2.0
#define SQUID_DECAY_THETA (1.0/64 -1.0/128)

#define SQUID_DIVE_SPEED (1.0/16)
#define SQUID_DIVE_ANGLE (1.0/16)

#define SQUID_ANGLE_MIN -M_PI*0.3
#define SQUID_ANGLE_MAX M_PI/4

#define SQUID_WAVE_TOLERANCE 0.0

squid_t* squid = NULL;
joypad_buttons_t buttons = {0};

squid_t* squid_init(void) {
	squid_t* new_squid = malloc(sizeof(squid_t));
	new_squid->x = 50.0;
	new_squid->y = 20.0;
	new_squid->momentum = 0.5;
	new_squid->time_in_air = 0;
	new_squid->theta = 0.0;
	new_squid->sprite = sprite_load("rom:/images/squid.sprite");
	new_squid->slice = 0;
	new_squid->blitparms = (rdpq_blitparms_t){
		.width = SQUID_SLICE_X,
		.height = SQUID_SLICE_Y,
		.cx = 16,
		.cy = 16,
	};

	return new_squid;
}

void squid_handle_input(void) {
	joypad_poll();
	buttons = joypad_get_buttons(JOYPAD_PORT_1);
}

void squid_set_slice(squid_t* s, uint8_t slice) {
	assertf(slice < SQUID_SLICE_MAX, "Squid slice value too high!");
	s->blitparms.s0 = (slice%(s->sprite->width/SQUID_SLICE_X))*SQUID_SLICE_X;
	s->blitparms.t0 = (slice/(s->sprite->height/SQUID_SLICE_Y))*SQUID_SLICE_Y;
}

void squid_move(squid_t* s) {
	// Calculate the Cartesian coordinates from the polar coordinates
	float speed_x = s->momentum * cos(s->blitparms.theta);
	float speed_y = -s->momentum * sin(s->blitparms.theta);
	static float diff = 0.0;	// Difference in angles
	uint8_t type =				// The cycle number of the wave segment we're on
		(int)waves_get_value(s->x, WAVE_VALUE_TYPE);
	float waves_y =				// The y-position of the waves where the squid is at
		waves_get_value(s->x + speed_x, WAVE_VALUE_POS_Y);
	float waves_theta =			// The angle of the waves where the squid is at
		waves_get_value(s->x, WAVE_VALUE_THETA);

	// Check where the squid is going to be in the next frame
	// Is he in the air or on the wave?
	if (waves_y - SQUID_WAVE_TOLERANCE < s->y + speed_y) {
		// Squid is on the wave
		// Set the squid to stick onto the wave
		s->y = waves_y;

		// Check if we just impacted hard
		if (diff > M_PI/4 || s->momentum < 1.0) {
			// Slow us down to the minimum speed
			s->momentum = SQUID_MIN_MOMENTUM;
		}

		// Check what kind of segment we're on
		if (type == 1 || type == 2) {
			// Going down
			// Set the squid's theta to its theoretical next position
			s->blitparms.theta = waves_get_value(s->x , WAVE_VALUE_THETA);
		} else {
			// Going up
			// Struggling a bit to go uphill, slow him down
			if (s->momentum >= SQUID_MIN_MOMENTUM) {
				s->momentum -= SQUID_DECAY_MOMENTUM_UPHILL;
			}
			// Set theta to the current wave position
			s->blitparms.theta = waves_theta;

		}
		// Reset time in air
		s->time_in_air = 0;
	} else {
		// Squid is in the air
		if (s->time_in_air > 2) {
			// Flying normally
			s->y += speed_y;
			s->blitparms.theta = s->blitparms.theta <= SQUID_ANGLE_MIN ? SQUID_ANGLE_MIN : s->blitparms.theta - SQUID_DECAY_THETA;
		} else if (type >= 1 || s->momentum <= SQUID_MIN_MOMENTUM) {
			// Handle the mini-takeoff going downhill or uphill too slowly
			s->y = waves_y;
			s->blitparms.theta = waves_theta;
		} else {
			// Let's takeoff normally
			s->y += speed_y;
		}
		// Update some variables to record the state of our flight
		diff = fabs(s->blitparms.theta - waves_theta);
		s->time_in_air++;
	}

	// Adjust the squid's dive when A is pressed
	if (buttons.a) {
		s->blitparms.theta -= SQUID_DIVE_ANGLE;
		s->momentum += SQUID_DIVE_SPEED;
	}

	return;
	// Debug crap
	rdpq_text_printf(NULL, 1, 20, 20, "Speeds: %.2f, %.2f", speed_x, speed_y);
	rdpq_text_printf(NULL, 1, 20, 60, "Momentum: %f", s->momentum);
	rdpq_text_printf(NULL, 1, 20, 80, "In air: %li", s->time_in_air);

	rdpq_set_mode_fill(RGBA32(200, 0, 0, 0));
	rdpq_fill_rectangle(
			s->x - 1,
			s->y - 1,
			s->x + 1,
			s->y + 1
		);
	rdpq_fill_rectangle(
			s->x - 1 + speed_x * 10,
			s->y - 1 + speed_y * 10,
			s->x + 1 + speed_x * 10,
			s->y + 1 + speed_y * 10
		);
}

void squid_draw(squid_t* s) {
	assertf(s != NULL, "Squid not set!");
	static uint16_t i = 0;
	i++;
	squid_set_slice(s, i/10%3);

	rdpq_set_mode_standard();
	rdpq_mode_blender(RDPQ_BLENDER_MULTIPLY);
	rdpq_sprite_blit(s->sprite, s->x+WAVE_OFFSET_X, s->y, &s->blitparms);

	rdpq_text_printf(NULL, 1, 20, 20, "Input: %x", buttons.raw);
}

void squid_delete(squid_t** s) {
	free(*s);
	*s = NULL;
}
