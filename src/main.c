#include <libdragon.h>
#include <math.h>
#include <stdlib.h>
#include "squid.h"
#include "waves.h"

void my_print_function(const char *format, ...) {
	va_list args;
	va_start(args, format);
	rdpq_text_vprintf(
		NULL, 1, 20.0, 80.0,
		format, // Format string
		args // Variable argument list data type
		);
	va_end(args);
}

int main(void)
{
	// Initialise the various systems
	display_init(RESOLUTION_320x240, DEPTH_16_BPP, 3, GAMMA_NONE, FILTERS_RESAMPLE);
	dfs_init(DFS_DEFAULT_LOCATION);
	rdpq_init();
	joypad_init();

	rdpq_font_t *fnt1 = rdpq_font_load_builtin(FONT_BUILTIN_DEBUG_MONO);
	rdpq_text_register_font(1, fnt1);

	squid = squid_init();
	waves_init();

	uint32_t l = 0;

	// Main loop
	while(1) {
		// Start a new frame
		// Get the frame buffer
		surface_t* disp;
		while(!(disp = display_try_get()));

		// Attach the buffer to the RDP (No z-buffer needed yet)
		rdpq_attach_clear(disp, NULL);

		// Get controller input
		squid_handle_input();


		rdpq_set_mode_fill(RGBA32(128, 128, 128, 0));
		rdpq_fill_rectangle(0, 0, display_get_width(), display_get_height());

		// Calculate movement
		squid_move(squid);

		// Draw elements to screen
		waves_draw();
		squid_draw(squid);

		// Send frame buffer to display (TV)
		rdpq_detach_show();
		l++;
	}
}
/*
typedef struct rdpq_textparms_s {
	int16_t style_id;
	int16_t width;
	int16_t height;
	rdpq_align_t align;
	rdpq_valign_t valign;
	int16_t indent;
	int16_t char_spacing;
	int16_t line_spacing;
	rdpq_textwrap_t wrap;
	int16_t *tabstops;
	bool disable_aa_fix;
	bool preserve_overlap;
} rdpq_textparms_t;
*/
