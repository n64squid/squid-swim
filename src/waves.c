#include "waves.h"
#include "squid.h"

#define WAVE_BORDER_WIDTH 5
#define SINE_PRECISION 128
#define SEGMENT_WIDTH SINE_PRECISION
#define SEGMENT_HEIGHT 100//SINE_PRECISION

#define SCALE_X 0.75f
#define SCALE_Y 0.5f

static float sine[SINE_PRECISION];
static uint8_t cycle = 0;

wave_ll_t waves_ll = {
	.start = NULL,
	.end = NULL,
};

// Returns the sine index position of pixel x in wave segment s
static uint8_t waves_get_sine_index(wave_segment_t* s, uint16_t x) {
	return (SINE_PRECISION - 1) * ((s->type%2)) + ((int)(x/s->scale_x)%SINE_PRECISION) * ((s->type%2)*-2+1);
}
// Returns the vertical offset of the wave at pixel x in wave segment s
static float waves_get_y_offset(wave_segment_t* s, uint16_t x) {
	return sine[waves_get_sine_index(s, x)] * SEGMENT_HEIGHT * (((s->type)/2)*2-1) * s->scale_y;
}

// Gets the position of the waves at any point X
float waves_get_value(float x, wave_value_t type) {
	assertf(waves_ll.start != NULL, "waves_ll.start is NULL!");
	wave_segment_t* current = waves_ll.start;
	// Loop though segments
	while (current != NULL) {
		// Check if this is the correct segment
		if (x < current->pos_x + SEGMENT_WIDTH * current->scale_x) {
			// It is!
			if (type == WAVE_VALUE_POS_Y) {
				return current->pos_y + waves_get_y_offset(current, x - current->pos_x);
			} else if (type == WAVE_VALUE_THETA) {
				return sine[SINE_PRECISION-waves_get_sine_index(current, x - current->pos_x)-1]
					* (current->scale_y / current->scale_x)
					* (current->type == 0 || current->type == 3 ? 1 : -1);
			} else if (type == WAVE_VALUE_TYPE) {
				return current->type;
			}

		} else {
			// It isn't... Skip it
			current = current->next;
		}
	}
	return 0.0;
}

void waves_init(void) {
	// Calculate sine a bunch of times
	for (int i=0; i<SINE_PRECISION; i++) {
		sine[i] = sin((M_PI/SINE_PRECISION/2)*i);
	}

	// Create some segments
	for (int i=0; i<5; i++) {
		waves_add(&waves_ll);
	}
}

void waves_add (wave_ll_t* w) {
	// Create a new segment
	wave_segment_t* s = malloc(sizeof(wave_segment_t));

	// Populate the segment
	if (s != NULL) {

		if (w->end == NULL) {
			w->start = s;
			s->scale_x = SCALE_X;
			s->scale_y = SCALE_Y;
			s->pos_x = 0.0f;
			s->pos_y = 150.0f;
			s->type = cycle%4;
			s->next = NULL;
		} else {
			w->end->next = s;
			s->scale_x = SCALE_X; // Must be a sum of powers of 2
			s->scale_y = w->end->type%2 ? w->end->scale_y : w->end->scale_y + 0.05*(TICKS_READ()%3) - 0.05*(rand()%3);
			s->scale_y = s->scale_y > 1.0 ? 1.0 : (s->scale_y <= 0.4 ? 0.4 : s->scale_y);
			s->pos_x = w->end->pos_x + SEGMENT_WIDTH * w->end->scale_x - WAVE_OFFSET_X;
			s->pos_y = w->end->pos_y + (w->end->type%2 ? 0 : SEGMENT_HEIGHT * (w->end->scale_y - s->scale_y) * ((w->end->type/2)*2-1));
			s->type = cycle%4;
			s->next = NULL;
		}
		w->end = s;
		cycle++;
	}
}

void waves_draw_segment (wave_segment_t* s) {
	int16_t y_offset;							// Precalculated y offset
	int16_t pos_x_rounded = (int)s->pos_x;		// Made into an int to prevent flickering
	rdpq_set_mode_fill(RGBA32(0, 0, 0, 0));
	// Loop through all 1-pixel segments
	for (int16_t i=0; i<SEGMENT_WIDTH*s->scale_x; i++) {
		// Calculate the y offset of this segment
		y_offset = waves_get_y_offset(s, i);
		// Draw the segment
		rdpq_fill_rectangle(
			pos_x_rounded + i,
			s->pos_y + y_offset,
			pos_x_rounded + i + 1,
			s->pos_y + y_offset + WAVE_BORDER_WIDTH
		);
	}
	rdpq_set_mode_fill(RGBA32(255, 255, 255, 0));
	// Loop through all 1-pixel segments
	for (int16_t i=0; i<SEGMENT_WIDTH*s->scale_x; i++) {
		// Calculate the y offset of this segment
		y_offset = waves_get_y_offset(s, i);
		// Draw the segment
		rdpq_fill_rectangle(
			pos_x_rounded + i,
			s->pos_y + y_offset+WAVE_BORDER_WIDTH,
			pos_x_rounded + i + 1,
			display_get_height()
		);
	}

}

void waves_draw(void) {
	assertf(waves_ll.start != NULL, "waves_ll.start is NULL!");
	// Keep track of the current node
	wave_segment_t* current = waves_ll.start;
	int i = 0;
	float speed_x = squid->momentum * cos(squid->blitparms.theta);
	// Check to see if it's off-screen
	if (waves_ll.start->pos_x < -SEGMENT_WIDTH * waves_ll.start->scale_x) {
		// Delete it
		waves_ll.start = waves_ll.start->next;
		free (current);
		current = waves_ll.start;
	}
	// Draw the border
	while (current != NULL) {
		// Draw the segment
		waves_draw_segment (current);
		// Move it leftward
		current->pos_x -= speed_x;
		i++;
		current = current->next;
	}
	// check to add a new wave segment
	if (waves_ll.end->pos_x < display_get_width()-SEGMENT_WIDTH * waves_ll.end->scale_x) {
		waves_add(&waves_ll);
	}
}
