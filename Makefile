V=1
GAME=squidswim
SOURCE_DIR=src
BUILD_DIR=build
FILESYSTEM_DIR=$(SOURCE_DIR)/filesystem
IMAGE_DIR=$(SOURCE_DIR)/images

ASEPRITE_FILES=$(wildcard $(IMAGE_DIR)/*.ase)
ASEPRITE_PNGS=$(ASEPRITE_FILES:.ase=.png)
ASEPRITE_PNGS_CI4=$(filter $(IMAGE_DIR)/ci4/%.png, $(ASEPRITE_PNGS))
ASEPRITE_PNGS_RGBA16=$(filter $(IMAGE_DIR)/rgba16/%.png, $(ASEPRITE_PNGS))

IMAGE_FILESYSTEM=$(FILESYSTEM_DIR)/images
IMAGE_FILES=$(wildcard $(IMAGE_DIR)/*.png) $(ASEPRITE_PNGS_RGBA16)
IMAGE_SPRITES=$(sort $(subst src,src/filesystem,$(IMAGE_FILES:.png=.sprite)))

FONTS_DIR=$(SOURCE_DIR)/fonts
FONTS_FILESYSTEM=$(FILESYSTEM_DIR)/fonts
FONTS_FILES=$(wildcard $(FONTS_DIR)/*.ttf)
FONTS_FONTS64=$(subst fonts,filesystem/fonts,$(FONTS_FILES:.ttf=.font64))

MKFONT_FLAGS=--outline 1

GAP=5
NUM_COPIES=5
IMAGES = $(foreach n, $(shell seq 0 $(NUM_COPIES)), image$(n).png)

include $(N64_INST)/include/n64.mk

all: $(GAME).z64
.PHONY: all
OBJS = $(addprefix $(BUILD_DIR)/,$(notdir $(subst .c,.o,$(wildcard $(SOURCE_DIR)/*.c))))

$(IMAGE_FILESYSTEM)/%.sprite: $(IMAGE_DIR)/%.png
	@mkdir -p $(dir $@)
	@echo "    [SPRITE RGBA16] $@"
	$(N64_INST)/bin/mksprite $(MKSPRITE_FLAGS) -o $(dir $@) $<

$(ASEPRITE_PNGS): $(ASEPRITE_FILES)
	@echo "    [ASEPRITE] $(patsubst %.png,%.ase,$@) $(notdir $@)"
	cd $(dir $@) && aseprite $(notdir $(patsubst %.png,%.ase,$@)) -b --sheet-pack --sheet $(notdir $@) > /dev/null

$(FONTS_FILESYSTEM)/%.font64: $(FONTS_DIR)/%.ttf
	@mkdir -p $(dir $@)
	@echo "    [FONT] $@"
	$(N64_INST)/bin/mkfont $(MKFONT_FLAGS) -o $(FONTS_FILESYSTEM) "$<"

# Complete compiling
$(IMAGE_FILESYSTEM): $(IMAGE_SPRITES) $(ASEPRITE_PNGS)
$(FONTS_FILESYSTEM): $(FONTS_FONTS64)
$(BUILD_DIR)/$(GAME).dfs: $(IMAGE_FILESYSTEM) $(FONTS_FILESYSTEM)
$(BUILD_DIR)/$(GAME).elf: $(OBJS)

$(GAME).z64: N64_ROM_TITLE="Squid Swim"
$(GAME).z64: $(BUILD_DIR)/$(GAME).dfs

clean:
	@echo $(IMAGE_CI4_SPRITES)
	rm -f $(BUILD_DIR)/*
	rm -f $(GAME).z64
	rm -rf $(FILESYSTEM_DIR)
	rm -f $(ASEPRITE_PNGS)
.PHONY: clean

test:
	@echo "MIAU" $(ASEPRITE_FILES)
.PHONY: test
-include $(wildcard $(BUILD_DIR)/*.d)
